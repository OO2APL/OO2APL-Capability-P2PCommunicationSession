package p2pCommunicationCapability.triggers;

import oo2apl.agent.AgentID;
import oo2apl.agent.Trigger;
import p2pCommunicationCapability.CommunicationSessionId;
/**
 * Not final because you can extend invitations for different session types.
 * @author bas
 *
 */
public class SessionInvitation implements Trigger {
	private final AgentID inviter;
	private final CommunicationSessionId sessionId; 
	
	protected SessionInvitation(final AgentID inviter, final CommunicationSessionId sessionId){
		this.inviter = inviter;
		this.sessionId = sessionId;
	}

	public final AgentID getInviter() {
		return this.inviter;
	}

	public final CommunicationSessionId getSessionId() {
		return this.sessionId;
	}
	
}
