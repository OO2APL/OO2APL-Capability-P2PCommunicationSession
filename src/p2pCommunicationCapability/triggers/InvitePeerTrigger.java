package p2pCommunicationCapability.triggers;

import oo2apl.agent.AgentID;
import oo2apl.agent.Trigger;
import p2pCommunicationCapability.CommunicationSessionId;

public class InvitePeerTrigger implements Trigger {
	private final AgentID peerToInvite;
	private final CommunicationSessionId sessionId; 
	
	public InvitePeerTrigger(final AgentID peerToInvite, final CommunicationSessionId sessionId){
		this.peerToInvite = peerToInvite;
		this.sessionId = sessionId;
	}

	public final AgentID getPeerToInvite() {
		return this.peerToInvite;
	}

	public final CommunicationSessionId getSessionId() {
		return this.sessionId;
	}
	
	public SessionInvitation getInvitation(final AgentID inviter, final CommunicationSessionId sessionId){
		return new SessionInvitation(inviter, sessionId);
	}
	
}
