package p2pCommunicationCapability.triggers;

import oo2apl.agent.AgentID;
import oo2apl.agent.Trigger;

public final class InvitationReject implements Trigger {
	private final SessionInvitation invitation;
	private final AgentID peer;
	
	public InvitationReject(final SessionInvitation invitation, final AgentID peer){
		this.invitation = invitation;
		this.peer = peer;
	}
	
	public final SessionInvitation getInvitation(){
		return this.invitation;
	}
	
	public final AgentID getPeer(){
		return this.peer;
	}
}
