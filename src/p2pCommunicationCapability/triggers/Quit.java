package p2pCommunicationCapability.triggers;

import oo2apl.agent.AgentID;
import oo2apl.agent.Trigger;
import p2pCommunicationCapability.CommunicationSessionId;

public final class Quit implements Trigger {
	private final AgentID peerThatQuits;
	private final CommunicationSessionId sessionId;
	
	public Quit(final AgentID peerThatQuits, final CommunicationSessionId sessionId){
		this.peerThatQuits = peerThatQuits;
		this.sessionId = sessionId;
	}
	
	public final AgentID getPeerThatQuits(){ 
		return this.peerThatQuits;
	}
	
	public final CommunicationSessionId getSessionId(){
		return this.sessionId;
	}
}
