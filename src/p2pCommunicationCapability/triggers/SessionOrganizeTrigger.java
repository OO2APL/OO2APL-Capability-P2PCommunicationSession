package p2pCommunicationCapability.triggers;
 
import oo2apl.agent.AgentID;
import oo2apl.agent.Trigger;
import p2pCommunicationCapability.CommunicationSessionId;
/**
 * 
 * 
 * Not final since the trigger can be extended for different types of dialogs.
 * @author bas
 *
 */
public class SessionOrganizeTrigger implements Trigger {
	private final AgentID[] peers; 
 
	public SessionOrganizeTrigger(final AgentID[] peers){
		this.peers = peers; 
	}
	
	public final AgentID[] getPeers(){
		return this.peers;
	}
	
	public SessionInvitation getInvitation(final AgentID inviter, final CommunicationSessionId sessionId){
		return new SessionInvitation(inviter, sessionId);
	}
}
