package p2pCommunicationCapability.triggers;

import oo2apl.agent.AgentID;
import oo2apl.agent.Trigger;

public final class InvitationAccept implements Trigger {
	private final SessionInvitation invitation;
	private final AgentID peer;
	
	public InvitationAccept(final SessionInvitation invitation, final AgentID peer){
		this.invitation = invitation;
		this.peer = peer;
	}
	
	public final SessionInvitation getInvitation(){
		return this.invitation;
	}
	
	public final AgentID getPeer(){
		return this.peer;
	}
}
