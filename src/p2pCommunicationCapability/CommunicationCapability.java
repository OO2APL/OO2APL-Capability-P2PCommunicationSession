package p2pCommunicationCapability;

import java.util.Iterator;
import java.util.function.BiConsumer;
import java.util.function.BiPredicate;
 
import oo2apl.agent.AgentBuilder;
import oo2apl.agent.AgentID;
import oo2apl.agent.PlanToAgentInterface;
import oo2apl.agent.Trigger;
import oo2apl.plan.builtin.FunctionalPlanSchemeInterface;
import oo2apl.plan.builtin.SubPlanInterface;
import p2pCommunicationCapability.triggers.InvitationAccept;
import p2pCommunicationCapability.triggers.InvitationReject;
import p2pCommunicationCapability.triggers.InvitePeerTrigger;
import p2pCommunicationCapability.triggers.Quit;
import p2pCommunicationCapability.triggers.SessionInvitation;
import p2pCommunicationCapability.triggers.SessionOrganizeTrigger;
/**
 * Session initialize triggers are assumed to be adoped internally when the agent has decided that it wants 
 * to organize/join the session. Hence, decision logic on for instance deciding to join a session should be 
 * executed prior to adop
 * @author bas
 *
 */
public final class CommunicationCapability extends AgentBuilder {
	private final BiPredicate<SessionInvitation, PlanToAgentInterface> acceptInvitationCondition;
	private final OrganizeEventListener onOrganize;
	private final CommunicationSessionEventListener onAccept, onReject, onQuit;

	public CommunicationCapability(
			final OrganizeEventListener onOrganize,
			final BiPredicate<SessionInvitation, PlanToAgentInterface> acceptInvitationCondition,
			final CommunicationSessionEventListener onAccept,
			final CommunicationSessionEventListener onReject,
			final CommunicationSessionEventListener onQuit){
		this.onOrganize = onOrganize;
		this.acceptInvitationCondition = acceptInvitationCondition;
		this.onAccept = onAccept;
		this.onReject = onReject;
		this.onQuit = onQuit;
		
		// Context for managing commmunication sessions
		super.addContext(new SessionContext());

		// Plan scheme for when the agent decides to organize a communication session
		super.addInternalTriggerPlanScheme(onTriggerTypeScheme(SessionOrganizeTrigger.class, this::organizeSession));
		// Plan scheme for when the agent decides to invite a peer to an existing communication session
		super.addInternalTriggerPlanScheme(onTriggerTypeScheme(InvitePeerTrigger.class, CommunicationCapability::invitePeer));
		// Plan scheme for when the agent decides to quit a communication session
		super.addInternalTriggerPlanScheme(onTriggerTypeScheme(Quit.class, CommunicationCapability::quit));
		
		// Scheme for handling an invitation to join a session
		super.addMessagePlanScheme(onTriggerTypeScheme(SessionInvitation.class, this::handleInvitation));
		// Scheme for handling a peer accepting an invitation
		super.addMessagePlanScheme(onTriggerTypeScheme(InvitationAccept.class, this::handleAccept));
		// Scheme for handling a peer rejecting an invitation
		super.addMessagePlanScheme(onTriggerTypeScheme(InvitationReject.class, this::handleReject));
		// Scheme for handling a peer quitting a session
		super.addMessagePlanScheme(onTriggerTypeScheme(Quit.class, this::handleQuit));
	}
	
	/** Create a plan scheme that triggers for each trigger that is an instanceof the given class and executes the given BiConsumer. */
	private static final FunctionalPlanSchemeInterface onTriggerTypeScheme(final Class<? extends Trigger> klass, final BiConsumer<Trigger, PlanToAgentInterface> plan){
		return (trigger, contextInterface) -> {
			if(klass.isInstance(trigger)) return (planInterface)->{ plan.accept(trigger, planInterface); };
			else return SubPlanInterface.UNINSTANTIATED;
		};
	}
	
	/// Internal plan schemes

	private final void organizeSession(final Trigger trigger, final PlanToAgentInterface planInterface) {
		// Create the session
		SessionOrganizeTrigger organizeTrigger = (SessionOrganizeTrigger) trigger;
		SessionContext context = planInterface.getContext(SessionContext.class); 
		CommunicationSessionId sessionId = context.newSession(planInterface.getAgentID()); 
		// Send invite to agents in the trigger
		for(AgentID peer : organizeTrigger.getPeers()){
			planInterface.sendMessage(peer, organizeTrigger.getInvitation(planInterface.getAgentID(), sessionId)); 
			context.addInvitedPeer(sessionId, peer);
		}
		this.onOrganize.notify(organizeTrigger, sessionId, planInterface);
	}
	
	private static final void invitePeer(final Trigger trigger, final PlanToAgentInterface planInterface) {
		// Create the session
		InvitePeerTrigger invitePeerTrigger = (InvitePeerTrigger) trigger;
		SessionContext context = planInterface.getContext(SessionContext.class); 
		// Send invite to agents in the trigger
		planInterface.sendMessage(invitePeerTrigger.getPeerToInvite(), 
				invitePeerTrigger.getInvitation(planInterface.getAgentID(), invitePeerTrigger.getSessionId()));
		context.addInvitedPeer(invitePeerTrigger.getSessionId(), invitePeerTrigger.getPeerToInvite());		  
	}
	
	private static final void quit(final Trigger trigger, final PlanToAgentInterface planInterface) {
		Quit quitTrigger = (Quit) trigger; 
		SessionContext context = planInterface.getContext(SessionContext.class); 
		for(Iterator<AgentID> peers = context.quitFromSession(quitTrigger.getSessionId()); peers.hasNext();){
			planInterface.sendMessage(peers.next(), new Quit(planInterface.getAgentID(), quitTrigger.getSessionId()));
		} 
	}
	
	/// Message plan schemes

	private final void handleInvitation(final Trigger trigger, final PlanToAgentInterface planInterface) {
		SessionInvitation invitation = (SessionInvitation) trigger;  
		// Send acknowledgement to the agent that sent the invite   
		if(this.acceptInvitationCondition.test(invitation, planInterface)){ 
			planInterface.sendMessage(invitation.getInviter(), new InvitationAccept(invitation, planInterface.getAgentID()));
			SessionContext context = planInterface.getContext(SessionContext.class); 
			context.joinSession(invitation.getSessionId()); // Remember that you joined the session
			context.addAcceptedPeer(invitation.getSessionId(), invitation.getInviter()); // Inviter always counts-as an accepted peer
		} else { 
			planInterface.sendMessage(invitation.getInviter(), new InvitationReject(invitation, planInterface.getAgentID()));
		}
	}
	
	private final void handleAccept(final Trigger trigger, final PlanToAgentInterface planInterface) {
		InvitationAccept response = (InvitationAccept) trigger; 
		SessionContext context = planInterface.getContext(SessionContext.class);
		context.addAcceptedPeer(response.getInvitation().getSessionId(), response.getPeer());
		this.onAccept.notify(response.getInvitation().getSessionId(), response.getPeer(), planInterface);
	}

	private final void handleReject(final Trigger trigger, final PlanToAgentInterface planInterface) {
		InvitationReject response = (InvitationReject) trigger; 
		SessionContext context = planInterface.getContext(SessionContext.class);
		context.addRejectedPeer(response.getInvitation().getSessionId(), response.getPeer()); 
		this.onReject.notify(response.getInvitation().getSessionId(), response.getPeer(), planInterface);
	}
	
	private final void handleQuit(final Trigger trigger, final PlanToAgentInterface planInterface) {
		Quit quitTrigger = (Quit) trigger; 
		SessionContext context = planInterface.getContext(SessionContext.class);
		context.handlePeerQuit(quitTrigger.getSessionId(), quitTrigger.getPeerThatQuits());
		this.onQuit.notify(quitTrigger.getSessionId(), quitTrigger.getPeerThatQuits(), planInterface);
	}
	
	public static interface CommunicationSessionEventListener {
		public final static CommunicationSessionEventListener DO_NOTHING = (sessionId, peer, planInterface) -> {};
		
		public void notify(final CommunicationSessionId sessionId, final AgentID peer, final PlanToAgentInterface planInterface);
	}
	
	public static interface OrganizeEventListener {
		public final static OrganizeEventListener DO_NOTHING = (organizeTrigger, sessionId, planInterface) -> {};
		
		public void notify(final SessionOrganizeTrigger organizeTrigger, final CommunicationSessionId sessionId, final PlanToAgentInterface planInterface);
	}
}
