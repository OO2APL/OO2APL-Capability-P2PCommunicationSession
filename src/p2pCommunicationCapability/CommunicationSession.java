package p2pCommunicationCapability;
 
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet; 
import java.util.Set; 

import oo2apl.agent.AgentID;

/**
 * Extendible class that maintains a session state for a single agent. 
 * 
 * 
 * @author Bas Testerink
 */
public class CommunicationSession {
	// Id that the agent uses to refer to this session
	private final CommunicationSessionId id;
	// Interactions with peers
	private final Set<AgentID> invitedPeers;
	private final Set<AgentID> rejectedPeers;
	private final Set<AgentID> acceptedPeers;
	private final Set<AgentID> quitPeers;
	
	public CommunicationSession(final CommunicationSessionId id){
		this.id = id;
		this.invitedPeers = new HashSet<>();
		this.rejectedPeers = new HashSet<>();
		this.acceptedPeers = new HashSet<>(); 
		this.quitPeers = new HashSet<>(); 
	}
	

	public final void addInvitedPeer(final AgentID peer){
		this.invitedPeers.add(peer);
	}
	
	public final void addAcceptedPeer(final AgentID peer){
		this.invitedPeers.remove(peer);
		this.acceptedPeers.add(peer);
	}
	
	public final void addRejectedPeer(final AgentID peer){
		this.invitedPeers.remove(peer);
		this.rejectedPeers.add(peer);
	}
	
	public final void addQuitPeer(final AgentID peer){
		this.acceptedPeers.remove(peer);
		this.quitPeers.add(peer);
	}
	
	
	public final boolean previouslyInvitedPeer(final AgentID peer){
		return this.rejectedPeers.contains(peer) || 
			this.acceptedPeers.contains(peer) || 
			this.invitedPeers.contains(peer) ||
			this.quitPeers.contains(peer);
	}
	
	public final boolean peerParticipates(final AgentID peer){
		return this.acceptedPeers.contains(peer);
	}

	public final CommunicationSessionId getId() {
		return this.id;
	}

	public final Collection<AgentID> getInvitedPeersUnmodifiable(){ 
		return Collections.unmodifiableCollection(this.invitedPeers);
	}

	public final Collection<AgentID> getRejectedPeersUnmodifiable(){ 
		return Collections.unmodifiableCollection(this.rejectedPeers);
	}

	public final Collection<AgentID> getAcceptedPeersUnmodifiable(){ 
		return Collections.unmodifiableCollection(this.acceptedPeers);
	}

	public final Collection<AgentID> getQuitPeersUnmodifiable(){ 
		return Collections.unmodifiableCollection(this.quitPeers);
	}  
}
