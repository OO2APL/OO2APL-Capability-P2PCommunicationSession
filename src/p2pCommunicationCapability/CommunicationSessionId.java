package p2pCommunicationCapability;
 
import java.util.UUID;

import oo2apl.agent.AgentID;

public final class CommunicationSessionId {
	private final AgentID organizer;
	private final UUID localIdAtOrganizer;
	
	public CommunicationSessionId(final AgentID organizer, final UUID localIdAtOrganizer){
		this.organizer = organizer;
		this.localIdAtOrganizer = localIdAtOrganizer;
	}
	
	public final boolean equals(final Object other){
		if(other == null) return false;
		else if(other instanceof CommunicationSessionId){
			CommunicationSessionId otherId = (CommunicationSessionId) other;
			return otherId.getLocalIdAtOrganizer().equals(this.localIdAtOrganizer)
					&& otherId.getOrganizer().equals(this.getOrganizer());
		}
		return false;
	}
 
	public UUID setValue(UUID value) { 
		return null;
	}
	
	protected final AgentID getOrganizer(){
		return this.organizer;
	}
	
	protected final UUID getLocalIdAtOrganizer(){
		return this.localIdAtOrganizer;
	}
	
}
