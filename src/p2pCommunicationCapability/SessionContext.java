package p2pCommunicationCapability;

import java.awt.List;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID; 

import oo2apl.agent.AgentID;
import oo2apl.agent.Context;
/**
 * This class maintains the communication sessions for the agent.
 * 
 * TODO:
 *  - Instead of map use a Cache for storing the session and handling 
 *    expired sessions.
 * @author Bas Testerink
 *
 */ 
public final class SessionContext implements Context{
	private final Map<CommunicationSessionId, CommunicationSession> sessions;
	
	public SessionContext(){
		this.sessions = new HashMap<>();
	}
	
	/** Create a new session and return the identifier. */
	public final CommunicationSessionId newSession(final AgentID organizer){
		CommunicationSessionId id = new CommunicationSessionId(organizer, UUID.randomUUID());
		while(this.sessions.containsKey(id))
			id = new CommunicationSessionId(organizer, UUID.randomUUID());
		this.sessions.put(id, new CommunicationSession(id));
		return id;
	}
	
	/** Create a new session and return the identifier. */
	public final CommunicationSessionId joinSession(final CommunicationSessionId id){
		if(this.sessions.containsKey(id)){
			// TODO: how to handle?
		} else {
			this.sessions.put(id, new CommunicationSession(id));
		}
		return id;
	}

	public final void addAcceptedPeer(final CommunicationSessionId sessionId, final AgentID peer) {
		CommunicationSession session = this.sessions.get(sessionId);
		if(session != null) // Could be null if already quit the session before the accept arrived
			session.addAcceptedPeer(peer);
	}

	public final void addRejectedPeer(final CommunicationSessionId sessionId, final AgentID peer) {
		CommunicationSession session = this.sessions.get(sessionId);
		if(session != null) // Could be null if already quit the session before the reject arrived
			session.addRejectedPeer(peer);
	}
	
	public final void addInvitedPeer(final CommunicationSessionId sessionId, final AgentID peer) {
		this.sessions.get(sessionId).addInvitedPeer(peer);
	}
	
	public final Iterator<AgentID> quitFromSession(final CommunicationSessionId sessionId){
		CommunicationSession session = this.sessions.remove(sessionId); // TODO: handle null value
		Collection<AgentID> agentsToNotify = new ArrayList<>(); 
		agentsToNotify.addAll(session.getAcceptedPeersUnmodifiable());
		agentsToNotify.addAll(session.getInvitedPeersUnmodifiable()); 
		return agentsToNotify.iterator();
	}
	
	public final void handlePeerQuit(final CommunicationSessionId sessionId, final AgentID peer){
		CommunicationSession session = this.sessions.get(sessionId);
		if(session != null) // Could be null if already quit the session before the accept arrived
			session.addQuitPeer(peer);
	}
	
	public final Collection<AgentID> getAcceptedPeers(final CommunicationSessionId sessionId){
		CommunicationSession session = this.sessions.get(sessionId);
		return session == null ? Collections.emptyList() : session.getAcceptedPeersUnmodifiable();
	}
	
}
